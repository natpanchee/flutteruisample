import 'package:flutter/material.dart';


class RowColPage extends StatefulWidget {
  @override
  _RowColPageState createState() => new _RowColPageState();
}

class _RowColPageState extends State<RowColPage> {
  bool _isRow = true;
  Widget buildContent() {
    if (_isRow) {
      return Row(
//        mainAxisAlignment: _mainAxisAlignment,
//        crossAxisAlignment: _crossAxisAlignment,
//        mainAxisSize: _mainAxisSize,
        children: [
          Icon(Icons.stars, size: 50.0),
          Icon(Icons.stars, size: 100.0),
          Icon(Icons.stars, size: 50.0),
        ],
      );
    } else {
      return Column(
//        mainAxisAlignment: _mainAxisAlignment,
//        crossAxisAlignment: _crossAxisAlignment,
//        mainAxisSize: _mainAxisSize,
        children: [
          Icon(Icons.stars, size: 50.0),
          Icon(Icons.stars, size: 100.0),
          Icon(Icons.stars, size: 50.0),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Rows and Columns"),
      ),
      body: Container(color: Colors.yellow, child: buildContent())
    );
  }

}