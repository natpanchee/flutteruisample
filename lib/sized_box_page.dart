import 'package:flutter/material.dart';

// This classes uses hard coded width and height
class ThreeRowsLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainAxisAlignment _mainAxisAlignment = MainAxisAlignment.start;
    CrossAxisAlignment _crossAxisAlignment = CrossAxisAlignment.stretch;
    MainAxisSize _mainAxisSize = MainAxisSize.max;

    return SingleChildScrollView(
        child: Column(
          mainAxisAlignment: _mainAxisAlignment,
          crossAxisAlignment: _crossAxisAlignment,
          mainAxisSize: _mainAxisSize,
          children: [
            SizedBox(
                width: 200.0,
                height: 400.0,
                child: Container(
                    child: Text("Box 1"), color: Color.fromARGB(255, 255, 255, 0))),
            SizedBox(
                width: 200.0,
                height: 100.0,
                child: Container(
                    child: Text("Box 2"),
                    color: Color.fromARGB(255, 255, 255, 255))),
            SizedBox(
                width: 200.0,
                height: 50.0,
                child: Container(
                    child: Text("Box 3"),
                    color: Color.fromARGB(255, 255, 0, 255))) //,
//        Icon(Icons.stars, size: 100.0),
//        Icon(Icons.stars, size: 50.0),
          ],
        ));
  }
}