import 'package:flutter/material.dart';


//This class uses Expanded
class ThreeRowsUsingExpanded extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _buildContent();
  }
}

Widget _buildContent() {
  return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
    Expanded(
        flex: 1,
        child: Container(color: Colors.blueGrey, child: Text("Oh HELLO"))),
    Expanded(
        flex: 1,
        child: Container(color: Colors.yellow, child: Text("Oh HELLO"))),
    Expanded(
        flex: 1,
        child: Container(color: Colors.orange, child: Text("Oh HELLO")))
  ]);
}