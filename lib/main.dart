import 'package:flutter/material.dart';
import 'package:flutter_app/main_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI Test',
      home: MainPage(),
      );

  }
}



