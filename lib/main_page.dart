import 'package:flutter/material.dart';
import 'package:flutter_app/expanded_page.dart';
import 'package:flutter_app/layout_type.dart';
import 'package:flutter_app/sized_box_page.dart';
import 'package:flutter_app/row_col_page.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => new _MainPageState();
}



class _MainPageState extends State<MainPage> {
  LayoutType _layoutSelection = LayoutType.rowColumn;

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: [
        _buildItem(icon: Icons.view_week, layoutSelection: LayoutType.pageView),
        _buildItem(
            icon: Icons.format_list_bulleted, layoutSelection: LayoutType.list),
        _buildItem(icon: Icons.view_day, layoutSelection: LayoutType.slivers),
        _buildItem(icon: Icons.gradient, layoutSelection: LayoutType.hero),
        _buildItem(icon: Icons.dashboard, layoutSelection: LayoutType.nested),
      ],
      onTap: _onSelectTab,
    );
  }

  BottomNavigationBarItem _buildItem(
      {IconData icon, LayoutType layoutSelection}) {
    String text = "TEST";
    return BottomNavigationBarItem(
      icon: Icon(
        icon,
        color: Color.fromARGB(255, 255, 255, 0),
      ),
      title: Text(
        text,
        style: TextStyle(
          color: Color.fromARGB(255, 255, 0, 0),
        ),
      ),
    );
  }

  void _onSelectTab(int index) {
    switch (index) {
      case 0:
        _onLayoutSelected(LayoutType.rowColumn);
        break;
      case 1:
        _onLayoutSelected(LayoutType.baseline);
        break;
      case 2:
        _onLayoutSelected(LayoutType.stack);
        break;
      case 3:
        _onLayoutSelected(LayoutType.expanded);
        break;
      case 4:
        _onLayoutSelected(LayoutType.padding);
        break;
    }
  }


  void _onLayoutSelected(LayoutType selection) {
    setState(() {
      _layoutSelection = selection;
    });
  }

  Widget buildBody() {
    switch(_layoutSelection) {
      case LayoutType.expanded:
        return ThreeRowsUsingExpanded();
      case LayoutType.rowColumn:
        return RowColPage();
      default:
        return ThreeRowsLayout();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
